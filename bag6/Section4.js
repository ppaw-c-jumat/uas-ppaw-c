import React, { Component } from "react";
// import './css/owl.carousel.min.css'
import './css/magnific-popup.css'
import './css/barfiller.css'
import './css/slicknav.min.css'
import "./style.css"
import bees from './img/bees.png';
import bugs from './img/bugs.png';
import insect from './img/insect.png';
import ants from './img/ants-2.png';
import mices from './img/mices.png';
import spiders from './img/spiders.png';
import roaches from './img/roaches.png';
import pentry from './img/pentry-pest.png';
import other from './img/other-pest.png';

class Section4 extends Component {
    render() {
        return (
            <div>
                <h2 class="vc_custom_heading vc_custom_1491856417723">WE TARGET</h2>
                <section class="center slider">
                    <div>
                        <a href="#" /><img title="BEES" src={bees} alt="gambar" /><center><a class="bugss">Bees</a></center>
                    </div>
                    <div>
                        <a href="#" /><img title="BED BUGS" src={bugs} alt="gambar" /><center><a class="bugss">Bed Bugs</a></center>
                    </div>
                    <div>
                        <a href="#" /><img title="INSECTS" src={insect} alt="gambar" /><center><a class="bugss">Insects</a></center>
                    </div>
                    <div>
                        <a href="#" /><img title="TERMITES" src={ants} alt="gambar" /><center><a class="bugss">Terminates</a></center>
                    </div>
                    <div>
                        <a href="#" /><img title="MOUSE & RODENTS" src={mices} alt="gambar" /><center><a class="bugss">Mouse & Rodents</a></center>
                    </div>
                    <div>
                        <a href="#" /><img title="SPIDERS" src={spiders} alt="gambar" /><center><a class="bugss">Spiders</a></center>
                    </div>
                    <div>
                        <a href="#" /><img title="ROACHES" src={roaches} alt="gambar" /><center><a class="bugss">Roaches</a></center>
                    </div>
                    <div>
                        <a href="#" /><img title="PANTRY PESTS" src={pentry} alt="gambar" /><center><a class="bugss">Pantry Pests</a></center>
                    </div>
                    <div>
                        <a href="#" /><img title="OTHER INSECTS" src={other} alt="gambar" /><center><a class="bugss">Other Insects</a></center>
                    </div>
                </section>
            </div>
        )
    }

}

export default Section4;