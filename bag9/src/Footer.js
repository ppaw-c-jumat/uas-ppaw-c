import React,{Component} from "react";
import Mouse1 from "./img/mouse1.png";
import Mouse2 from "./img/mouse2.png";
import Contact from "./img/contact.png";
import Sendmail from "./img/sendmail.png";
import facebook from "./img/facebook.png";
import twitter from "./img/twitter.png";
import linkedin from "./img/linkedin.png";
import pinterest from "./img/pinterest.png";
import googleplus from "./img/googleplus.png";

class Archive extends Component{
    render(){
        return (
            <div class="row">
                <div class="col-sm-3" id="Satu">
                    <h3>SUBSCRIBE</h3>
                        <br/>
                        <p>Enter your email address to suscribe to this
                        blog and receive notifications of new posts by email.</p>
                        <div class="emailbaru">
                        <div class="row">
                            <div className="col-md-6 col-lg-6 col-sm-6" >
                                <input className="suscribe" placeholder="Email Address" type="text" /> 
                            </div>
                            <div className="col-md-6 col-lg-6 col-sm-6">
                                <div className="email">
                                    <a href="/" className="tombol" >
                                        <img src={Sendmail} alt=""></img>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-3" id="Dua">
                    <h3>LASTEST BLOG</h3>
                    <br/>
                    <div className="blog1">
                        <img src={Mouse1} alt="" className="blogmouse" />
                        <a href="/">
                        <h3 className="mouse1">Top 3 reasons to treat for rodents this winter</h3>
                        <h3 className="mouse2">February 2,2017</h3>
                        </a>
                    </div>
                    <br/>
                    <div className="blog2">
                        <img src={Mouse2} alt="" className="blogmouse" />
                        <a href="/">
                            <h3 className="mouse1">Top 5 Signs There Might Be Rodents in your home</h3>
                            <h3 className="mouse2">February 2,2017</h3>
                        </a>
                    </div>
                </div>
                <div className="col-sm-3" id="Tiga">
                    <h3>CONTACT</h3>
                    <br/>
                    <img src={Contact} alt=""/>
                    <ol>
                        <ul>8525 SW 92nd Street</ul>
                        <br/>
                        <ul>monsterpestcontrol@live.com</ul>
                        <br/>
                        <ul>786-488-7235</ul>
                        <br/>
                        <ul>miamipestcontrolsolutions.com</ul>
                    </ol>
                </div>
                <div class="col-sm-3" id="Empat">
                    <h3>FOLLOW US</h3>
                    <ul>
                        <a href="/"><img className="sosmed" src={facebook} alt="" /></a>
                        <a href="/"><img className="sosmed" src={twitter} alt="" /></a>
                        <a href="/"><img className="sosmed" src={linkedin} alt="" /></a>
                        <a href="/"><img className="sosmed" src={pinterest} alt="" /></a>
                        <a href="/"><img className="sosmed" src={googleplus} alt="" /></a>
                    </ul>
                </div>
            </div>
        );
    }
}

function Copyright(){
    return(
            <div>
                <div className="row">
                    <div className="col-sm-2">
                        <span className="pojok1">Monster Pest Control</span>
                    </div>
                    <div className="col-sm-7">
                        <span className="pojok2">Copyright 2013 Monster Pest Control Solutions All Rights Reserved</span>
                    </div>
                    <div className="col-sm-3">
                        <span className="pojok3">Website Designed by Rocket Marketing & Design</span>
                    </div>
                </div>
            </div>
    );
}
class Footer extends Component{
    render(){
        return(
            <section id="Footer">
                <Archive/>
                <div class="garis"></div>
                <Copyright/>
            </section>
        );
    }
}

export default Footer;