import React, { Component } from "react";
import "./style.scss";

class Button extends Component {
  render() {
    return (
     <React.Fragment>
         <button className="btn btn-general">
            {this.props.text}
         </button>
     </React.Fragment>
    );
  }
}
 
export default Button;