import React, { Component } from "react";
import Header from "./Header";
import Section from "./Section1";
import Section2 from "./Section2";
import Section3 from "./Section3";
import Section4 from "./Section4";
import Section5 from "./Section5";
import Section6 from "./Section6";
import Telepon from "./Telephone";
import Footer from "./Footer";
import "./css/bootstrap.min.css";

class Main extends Component {
  render() {
    return (
      <div>
        <div className="badan">
          <Header></Header>
          <Section></Section>
          <Section2></Section2>
          <Section3></Section3>
          <Section4></Section4>
          <Section5></Section5>
          <Section6></Section6>
          <Telepon></Telepon>
          <Footer></Footer>
        </div>
      </div>

    );
  }
}

export default Main;