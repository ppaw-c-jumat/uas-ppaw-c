import React, { Component } from "react";
import Jumbotron from "./Jumbotron";
import Form from "./Form";
import "./style.css";
import "./css/bootstrap.min.css"


class Section extends Component {
    render() {
        return (
            <div>
                <div class="row" id="Bagian2">
                    <div class="col-sm-7" id="Container">
                        <Jumbotron></Jumbotron>
                    </div>
                    <div class="col-sm-5" id="bagianform">
                        <Form></Form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Section;