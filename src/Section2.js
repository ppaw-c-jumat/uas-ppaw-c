import React,{Component} from "react";
import Offer from "./offer.js";
import "./css/bootstrap.min.css";
import "./style.css";

class Section2 extends Component{
    render(){
        return (
            <div class="row">
                <div class="col-md-12" id="Offer">
                    <Offer/>
                </div>
            </div>
        )
    }
}

export default Section2;