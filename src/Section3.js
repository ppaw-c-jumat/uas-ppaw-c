import React,{Component} from "react";
import {HashRouter} from "react-router-dom";
import Image1 from "./img/satu.png";
import Image2 from "./img/dua.png";

class Section3 extends Component{
    render(){
        return(
            <HashRouter>
                <div className="serangga">
                    <h1>OUR SERVICES</h1>
                </div>
                <div className="row" id="Bagian5">
                    <div className="col-sm-6" id="Serangga">
                        <div className="serangga">
                            <h2 className="Residential">
                            <img src={Image1} alt="" />
                            <br/>RESIDENTIAL<br/>PEST CONTROL SERVICE</h2>
                        </div>
                    </div>
                    <div className="col-sm-6" id="Nyamuk">
                        <div className="nyamuk">
                            <h2 className="commersial">
                            <img src={Image2} alt="" />
                            <br/>COMMERSIAL<br/>PEST CONTROL</h2>
                        </div> 
                    </div>
                </div>
            </HashRouter>
        );
    }
}

export default Section3; 