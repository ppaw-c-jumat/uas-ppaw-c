import React,{Component} from "react";
import "./css/barfiller.css";
import "./css/bootstrap.min.css";
import "./css/magnific-popup.css";
import "./css/slicknav.min.css";
import "./css/style.css";
import icon1 from "./img/icon-1.png";
import icon2 from "./img/icon-2.png";
import icon3 from "./img/icon-3.png";
import icon4 from "./img/icon-4.png";
import icon5 from "./img/icon-5.png";

class Section5 extends Component{
    render(){
        return(
            <section className="services-section spad">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <div className="section-title">
                                <h2>Why Choose Us ?</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div className="col-lg-2 col-sm-4">
                            <div className="single-service">
                                <img src={icon1} alt="" />
                                <p>Well-trained and trustworthy inspectors and technicians.</p>
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-4">
                            <div className="single-service c-text">
                                <img src={icon2} alt="" />
                                <p>Courteous and reliable office and sales team.</p>
                            </div>
                        </div>
                        <div className="col-lg-2 col-sm-4">
                            <div className="single-service">
                                <img src={icon3} alt="" />
                                <p>Internationally accepted, environment-friendly and low-risk chemicals.</p>
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-4">
                            <div className="single-service">
                                <img src={icon4} alt="" />
                                <p>On-time scheduling system.</p>
                            </div>
                        </div>
                        <div className="col-lg-2 col-sm-4">
                            <div className="single-service">
                                <img src={icon5} alt="" />
                                <p>Flexible payment terms suited to your budget.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Section5;