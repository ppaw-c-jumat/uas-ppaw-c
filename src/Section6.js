import React,{Component} from "react";
import Image from "./img/maps.png";
import Image2 from "./img/photo.png";
import Image3 from "./img/quote.png";


class Map extends Component{
    render(){
        return(
        <div className="row" id="Map">
    	    <div className="col-lg-12">
    		    <div className="jumbotron jumbotron-fluid bg-white pb-2">
      			    <div className="container text-center">
        			    <h1 className="display-5">WE SERVICE MIAMI DADE COUNTY</h1>
      			    </div>
   	 		    </div>
   	 		    <div>
        		    <img src={Image} className="img-fluid pb-4" alt="Responsive image" />
    		    </div>
    	    </div>
        </div>
        );
    }
}
class Quote extends Component{
    render(){
        return(
            <div className="container pt-5">
                <div className="row">
                    <div className="col-4">
                        <div className="media">
                            <img className="mr-3 rounded-circle" src={Image2} alt="Generic placeholder image" />
                            <div className="media-body">
                                <h5 className="mt-0">Lidia Gonzales</h5>
                                Doral, Florida
                            </div>
                        </div>
                    </div>
                    <div class="col-8 text-center" alt="quote">
                        <img src={Image3} className="quote" />
                        <p><small className="text-muted">“Monster Pest Control came out for a major ant problem we were experiencing. We are very pleased with the professional service we received. We will be telling all of our friends about your company.
                        </small></p>
                        <p><small className="text-muted" alt="thanks">Thanks for a great job!"</small></p>
                    </div>
                </div>
            </div>
        );
    }
}

class Section6 extends Component{
    render(){
        return(
            <div>
                <Map/>    
                <Quote/>
            </div>
        );
    }
}

export default Section6;