import React from "react";
import Image from "./img/telephone.png";

function Telepon(){
    return <div className="row">
                <div className="col-sm-12" id="Telepon">
                    <h3 className="contact">
                        <img src={Image} alt="" />
                        <span className="calling">CALL FOR YOUR FREE INSPECTION!</span>
                        <span className="number">(786)488-7235</span>
                    </h3>
                </div>
            </div>
}

export default Telepon;