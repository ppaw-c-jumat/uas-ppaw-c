import React, { Component } from "react";
import "./css/bootstrap.min.css";
import "./style.css";
import Image from "./img/bg.png";

class Offer extends Component {
    render() {
        return (
            <div>
       		 <section className="offer">
		        <h1 className="title">
		            WELCOME TO MONSTER <br/>
		            PEST CONTROL
		        </h1>
		        <hr className="border-bottom" />
		        <p>
		            We offer great Pest Management Services, All natural and Eco-Friendly Pest Control. Low risk, <br/>
		            non-toxic
		            pesticides and do it yourself bug proofing approaches!
		        </p>
		        <br/>
		        <p>
		            We care about you, your family and also the environment. For over 10 years, we’ve also cared about <br/>
		            providing effective green pest control services to safeguard your home and workplace from <br/>
		            unwanted fleas, ticks, roaches, ants, rats, bees, might not solely be a nuisance, but may pose <br/>
		            dangerous threats to your home, health and safety.
		        </p>
		    </section>
		    <img width="100%" src={Image} alt="" />
            </div>
        )
    }
}

export default Offer;